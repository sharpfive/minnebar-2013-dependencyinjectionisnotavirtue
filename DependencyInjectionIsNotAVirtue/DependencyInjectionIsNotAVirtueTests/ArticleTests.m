//
//  DependencyInjectionIsNotAVirtueTests.m
//  DependencyInjectionIsNotAVirtueTests
//
//  Created by Jaim Zuber on 3/19/13.
//  Copyright (c) 2013 Sharp Five Software. All rights reserved.
//

#import "ArticleTests.h"
#import "Article.h"
#import "Article+dateStub.h"
#import "NSDate+dateStub.h"




@implementation ArticleTests

- (void)setUp
{
    // Set-up code here.
    //Setup swizzle
    
    self.originalDate = class_getClassMethod([NSDate class], @selector(date));
    self.swizzledDate = class_getClassMethod([NSDate class], @selector(dateStub));
    method_exchangeImplementations(self.originalDate, self.swizzledDate);
    
    [super setUp];
}

- (void)tearDown
{
    // Tear-down code here.
    
    //Remove the swizzle...
    
    method_exchangeImplementations(self.swizzledDate, self.originalDate);
    
    [super tearDown];
}

//- (void)testPublishedIsSet
//{
//    NSDate *expectedPublishDate = [NSDate date];
//    
//    Article *article = [[Article alloc] init];
//    
//    [article publish];
//    
//    // I wish I knew how to test you...
//    STAssertEqualObjects(expectedPublishDate, article.publishedAt, @"Published at should be at now");
//}

- (void)testPublishedIsSetWithDateSwizzle
{
    
    
    // This now calls my dateStub
    NSDate *expectedPublishDate = [NSDate date];
    
    Article *article = [[Article alloc] init];
    [article publish];
    
    // Pass pls...
    STAssertEqualObjects(expectedPublishDate, article.publishedAt, @"Published should be equal to date");
}

- (void)testPublishedIsSetWithDateSwizzle2
{
    //I'm the same thing as above...
    
    // This now calls my dateStub
    NSDate *expectedPublishDate = [NSDate date];
    
    Article *article = [[Article alloc] init];
    [article publish];
    
    // Pass pls...
    STAssertEqualObjects(expectedPublishDate, article.publishedAt, @"Published should be equal to date");
}


- (void)testNotPublished
{
    Article *article = [[Article alloc] init];
    
    //Setup swizzle
    Method original,swizzled;
    
    original = class_getInstanceMethod([Article class], @selector(todaysDate));
    swizzled = class_getInstanceMethod([Article class], @selector(dateStub));
    method_exchangeImplementations(original, swizzled);
    
    //Not publishing nothing
    //[article publish];
    
    STAssertNil(article.publishedAt, @"We haven't published yet");
}

@end
