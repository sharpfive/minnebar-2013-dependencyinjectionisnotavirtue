//
//  Article+dateStub.h
//  DependencyInjectionIsNotAVirtue
//
//  Created by Jaim Zuber on 3/20/13.
//  Copyright (c) 2013 Sharp Five Software. All rights reserved.
//

#import "Article.h"

@interface Article (dateStub)

- (NSDate*)dateStub;

@end
