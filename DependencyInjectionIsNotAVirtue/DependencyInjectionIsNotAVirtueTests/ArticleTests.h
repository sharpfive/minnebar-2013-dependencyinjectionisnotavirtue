//
//  DependencyInjectionIsNotAVirtueTests.h
//  DependencyInjectionIsNotAVirtueTests
//
//  Created by Jaim Zuber on 3/19/13.
//  Copyright (c) 2013 Sharp Five Software. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import <objc/runtime.h>

@interface ArticleTests : SenTestCase

@property Method originalDate, swizzledDate;

@end
