//
//  Article+dateStub.m
//  DependencyInjectionIsNotAVirtue
//
//  Created by Jaim Zuber on 3/20/13.
//  Copyright (c) 2013 Sharp Five Software. All rights reserved.
//

#import "Article+dateStub.h"

@implementation Article (dateStub)

- (NSDate*)dateStub
{
    return [NSDate dateWithTimeIntervalSince1970:1000000];
}

@end
