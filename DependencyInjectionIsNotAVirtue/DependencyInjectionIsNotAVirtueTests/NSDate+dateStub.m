//
//  NSDate+dateStub.m
//  DependencyInjectionIsNotAVirtue
//
//  Created by Jaim Zuber on 3/20/13.
//  Copyright (c) 2013 Sharp Five Software. All rights reserved.
//

#import "NSDate+dateStub.h"

@implementation NSDate (dateStub)

+ (NSDate*)dateStub
{
    return [NSDate dateWithTimeIntervalSince1970:10000000];
}

@end
