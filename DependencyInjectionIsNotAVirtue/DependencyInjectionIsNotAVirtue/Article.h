//
//  DependencyInjectionIsNotAVirtue.h
//  DependencyInjectionIsNotAVirtue
//
//  Created by Jaim Zuber on 3/19/13.
//  Copyright (c) 2013 Sharp Five Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Article : NSObject

@property NSDate *publishedAt;

- (void)publish;

@end
