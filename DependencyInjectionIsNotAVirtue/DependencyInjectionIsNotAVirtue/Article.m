//
//  DependencyInjectionIsNotAVirtue.m
//  DependencyInjectionIsNotAVirtue
//
//  Created by Jaim Zuber on 3/19/13.
//  Copyright (c) 2013 Sharp Five Software. All rights reserved.
//

#import "Article.h"

@implementation Article

- (void)publish
{
    self.publishedAt = [NSDate date];
}

@end
